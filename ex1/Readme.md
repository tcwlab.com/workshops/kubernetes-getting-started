# Exercise 1: Simple website

In this exercise, you will:
- publish a container image
- deploy it to Kubernetes
- make it accessible through the internet

#### Step 1: Create `index.html` file

Create a simple static `index.html`, that will be public available. Perhaps you want to start with this template:

    <!DOCTYPE html>
    <html lang="de">
      <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Title</title>
      </head>
      <body>
      </body>
    </html>

#### Step 2: Create docker image

Use your previous created `index.html` file as content of your webserver. Therefore create a `Dockerfile` based on:

    FROM nginx:alpine

#### Step 3: Make image accessible

Build and tag your docker image according to the DockerHub conventions:

     <YOUR_DOCKER_ID>/[IMAGE]:[TAG]

Having done so, push your image to DockerHub.


#### Step 4: Deploy webserver

Create a YAML file, that deploys your container to the Kubernetes Cluster.
Use [this link](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#creating-a-deployment) as a good
starting point.
Set `replicas` at least to 2 and deploy it using `kubectl apply -f <your-file>.yaml`

#### Step 5: Make webserver accessible from within the cluster

Write a YAML file, that creates a service for your deployment to the Kubernetes Cluster.
Use [this link](https://kubernetes.io/docs/concepts/services-networking/service/#defining-a-service) as a good
starting point.
Deploy it using `kubectl apply -f <your-file>.yaml`

#### Step 5: Make webserver accessible from the internet

Write a YAML file that creates a ingress controller for your IP and DNS name. Use this template as a starting point:

    apiVersion: networking.k8s.io/v1
    kind: Ingress
    metadata:
      name: nginx-ingress
      annotations:
        ingress.kubernetes.io/rewrite-target: /
    spec:
      ingressClassName: nginx
      rules:
        - host: <YOUR_NAME>.localdev.tcwlab.com
          http:
            paths:
              - path: /
                pathType: Prefix
                backend:
                  service:
                    name: nginx-svc
                    port:
                      number: 80


Deploy it using `kubectl apply -f <your-file>.yaml` and validate your Kubernetes Deployments by accessing your server
via browser: `http://<YOUR_NAME>.localdev.tcwlab.com/index.html`

[Overview](../Readme.md)
