# Exercise 0: prepare environment

In this exercise, you will:
- install the Kubernetes command line tool
- prepare Kubernetes for DNS/internet access
- install Lens as local administration tool

#### Step 1: install `kubectl`

Depending on your operating system, install `kubectl` according to the official documentation:

- [Install kubectl on Linux](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux)
- [Install kubectl on macOS](https://kubernetes.io/docs/tasks/tools/install-kubectl-macos)
- [Install kubectl on Windows](https://kubernetes.io/docs/tasks/tools/install-kubectl-windows)

As pre-flight check let's check, if everything was configured correctly:

    kubectl cluster-info
     Kubernetes control plane is running at https://kubernetes.docker.internal:6443
     CoreDNS is running at https://kubernetes.docker.internal:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
     To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.

    kubectl get no                                                                                        ─╯
      NAME             STATUS   ROLES           AGE   VERSION
      docker-desktop   Ready    control-plane   20m   v1.24.0

    kubectl version                                                                                       ─╯
      Client Version: version.Info{Major:"1", Minor:"23", GitVersion:"v1.23.5", GitCommit:"c285e781331a3785a7f436042c65c5641ce8a9e9", GitTreeState:"clean", BuildDate:"2022-03-16T15:51:05Z", GoVersion:"go1.17.8", Compiler:"gc", Platform:"darwin/amd64"}
      Server Version: version.Info{Major:"1", Minor:"24", GitVersion:"v1.24.0", GitCommit:"4ce5a8954017644c5420bae81d72b09b735c21f0", GitTreeState:"clean", BuildDate:"2022-05-03T13:38:19Z", GoVersion:"go1.18.1", Compiler:"gc", Platform:"linux/amd64"}


#### Step 2: Install `ingress`

In order to be able to access services on your Kubernetes cluster, the `ingress` is taking care of the http(s) access management.

###### Apply manifests

You can simply install it by doing this:

    kubectl apply -f https://gitlab.com/tcwlab.com/workshops/kubernetes-getting-started/-/raw/main/ex0/ingress.yaml

###### Pre-flight check

Let's wait for the service to be deployed correctly

    kubectl wait --namespace ingress-nginx \
      --for=condition=ready pod \
      --selector=app.kubernetes.io/component=controller \
      --timeout=120s

###### Local testing

Let's create a simple web server and the associated service:

    kubectl create deployment demo --image=httpd --port=80
    kubectl expose deployment demo

Then create an ingress resource. The following example uses an host that maps to localhost:

    kubectl create ingress demo-localhost --class=nginx --rule="localdev.tcwlab.com/*=demo:80"

At this point, if you access http://localdev.tcwlab.com:8080/, you should see an HTML page telling you "It works!".

If everything was successful, don't forget to remove the demo ressources:

    kubectl delete ingress demo-localhost
    kubectl delete service demo
    kubectl delete deployment demo

#### Step 3: Install Lens

This step is as easy as going to https://k8slens.dev/ and downloading the appropriate version :-)

[Overview](../Readme.md)
