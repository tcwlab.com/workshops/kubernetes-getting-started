# Exercise 3: Readiness, Liveness and Limits

In this exercise, you will:
- add a ReadinessProbe
- monitor a Pod with a LivenessProbe
- add CPU and Memory limits

#### Step 1: ReadinessProbe

We will use/extend your deployment from exercise 2, so feel free to copy your files here.
Add a ReadinessProbe to your deployment:

    readinessProbe:
      httpGet:
        path: /healthz
        port: 8080
        httpHeaders:
        - name: X-Custom-Header
          value: Awesome
      initialDelaySeconds: 3
      periodSeconds: 3

Which values seems to be a good choice?

#### Step 2: LivenessProbe

We will use/extend your deployment from exercise 2, so feel free to copy your files here.
Add a LivenessProbe to your deployment:

    livenessProbe:
      httpGet:
        path: /healthz
        port: 8080
        httpHeaders:
        - name: X-Custom-Header
          value: Awesome
      initialDelaySeconds: 3
      periodSeconds: 3

Which values seems to be a good choice?

#### Step 3: Memory Limits

Define MemoryLimits:

    resources:
      limits:
        memory: "200Mi"
      requests:
        memory: "100Mi"

...and CPULimits:

        resources:
          limits:
            cpu: 1.0
          requests:
            cpu: 10m

- Can these two types be merged in a way?
- Which are good values?

[Overview](../Readme.md)
