1. [ex0: Preparation](ex0/Readme.md)
1. [ex1: Simple website](ex1/Readme.md)
1. [ex2: Namespaces, Configuration and Secrets](ex2/Readme.md)
1. [ex3: Limits, Readiness and Liveness](ex3/Readme.md)
1. [ex4: Meshing](ex4/Readme.md)

