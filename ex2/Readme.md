# Exercise 2: Namespaces, Configuration and Secrets

In this exercise, you will:
- use namespaces
- add a ConfigMap
- create a secret

#### Step 1: Use namespace

- delete your deployment/service/ingress from exercise 1 from your Kubernetes cluster using `kubectl delete -f <your-file>.yaml`
- Check if all files in the `k8s` folder fit your needs.
- Create a new namespace using a YAML file and modify the provided YAML files in order to use the namespace.
- Rollout your deployment

#### Step 2: Configmaps

In order to activate `gzip` in our Nginx we need to pass a custom `nginx.conf` file to the Pod:

    user  nginx;
    worker_processes  auto;

    error_log  /var/log/nginx/error.log notice;
    pid        /var/run/nginx.pid;


    events {
        worker_connections  1024;
    }


    http {
        include       /etc/nginx/mime.types;
        default_type  application/octet-stream;

        log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                          '$status $body_bytes_sent "$http_referer" '
                          '"$http_user_agent" "$http_x_forwarded_for"';

        access_log  /var/log/nginx/access.log  main;

        sendfile        on;
        #tcp_nopush     on;

        keepalive_timeout  65;

        gzip  on;

        include /etc/nginx/conf.d/*.conf;
    }

Your job is to create a YAML file that creates a ConfigMap with the given content.

    apiVersion: v1
    data:
      file.name: |
        this
        is
        the
        content
    kind: ConfigMap
    metadata:
      name: name-of-the-configmap
      namespace: name-of-the-namespace

The `deployment.yaml` file has also to be extended so that the content is mounted to `/etc/nginx/nginx.conf` within the
container. An example YAML file is shown here:

    apiVersion: v1
    kind: Pod
    metadata:
      name: dapi-test-pod
    spec:
      containers:
        - name: test-container
          image: k8s.gcr.io/busybox
          command: [ "/bin/sh", "-c", "ls /etc/config/" ]
          volumeMounts:
          - name: config-volume
            mountPath: /etc/config
      volumes:
        - name: config-volume
          configMap:
            # Provide the name of the ConfigMap containing the files you want
            # to add to the container
            name: special-config
      restartPolicy: Never

#### Step 3: Add a secret

Create a second HTML file `secret.html` with your own content. In order to create a `secret.yaml` description, the
content has to be passed to `base64 -w0`:

     cat secret.html | base64 -w0
     MWYyZDFlMmU2N2Rm

Having done this step, the YAML file can be written:

    apiVersion: v1
    kind: Secret
    metadata:
      name: mysecret
    type: Opaque
    data:
      secret.html: MWYyZDFlMmU2N2Rm

Alternatively, you can create a Secret Using `kubectl create secret`:

    kubectl create secret generic mysecret --from-file=./secret.html
    secret "mysecret" created

This is an example of a pod that mounts a secret in a volume:

    apiVersion: v1
    kind: Pod
    metadata:
      name: mypod
    spec:
      containers:
      - name: mypod
        image: redis
        volumeMounts:
        - name: foo
          mountPath: "/etc/foo"
          readOnly: true
      volumes:
      - name: foo
        secret:
          secretName: mysecret

Make the new "secret" file `secret.html` available right next to `index.html`.

[Overview](../Readme.md)
