# Exercise 4: Tracing and Meshing

In this exercise, you will:
- install a mesh
- install basic monitoring
- see what happens inside

#### Step 1: Documentation

In this exercise you just have to [follow these instructions](https://linkerd.io/2.11/getting-started/).

[Overview](../Readme.md)
